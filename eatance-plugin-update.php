<?php
/*
Plugin Name: Eatance Plugin Update
Plugin URI: https://eatancepress.evdpl.com/en/
Description: This is Eatance Plugin Update.
Author: Eatance Team
Version: 1.0
Author URI: https://eatancepress.evdpl.com/en/
*/




require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitbucket.org/vasimmca2015/eatance-plugin-update/',
	__FILE__,
	'eatance-plugin-update'
);

//Optional: If you're using a private repository, create an OAuth consumer
//and set the authentication credentials like this:
//Note: For now you need to check "This is a private consumer" when
//creating the consumer to work around #134:
// https://github.com/YahnisElsts/plugin-update-checker/issues/134
// $myUpdateChecker->setAuthentication(array(
// 	'consumer_key' => '...',
// 	'consumer_secret' => '...',
// ));

//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch('dev-branch');